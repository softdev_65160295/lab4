/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table con;
    private Table table;

    public Game() {
        this.player1 = new Player('X');
        this.player2 = new Player('O');
        this.table = table;
    }

    public void newGame() {
        this.table = new Table(player1, player2);
        
    }

    public void play() {
        showWelcome();
            
            newGame();
            
            while (true) {
                showTable();
                showTurn();
                inputRowCol();
                if (table.checkWin()) {
                    showTable();
                    showInfo();
                    printContinue();
                    inputContinue();
                    if(table.checkContinue()){
                        newGame();
                    }
                    else{
                        break;
                    }

                }
                if (table.checkDraw()) {
                    showTable();
                    showInfo();
                    printContinue();
                    inputContinue();
                    if(table.checkContinue()){
                        newGame();
                    }
                    else{
                        break;
                    }
                }
                table.switchPlayer();
            }
            
            
            
            
            
        
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();

        }
    }

    private void showWelcome() {
        System.out.println("Welcome to OX game");

    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);

        while (true) {
            System.out.print("Please input Row Col :");
            int row = kb.nextInt();
            int col = kb.nextInt();
            if (table.checkBound(row, col)) {
                System.out.println("Please input number within 1-3");
            } else if (table.checkPosition(row, col)) {
                System.out.println("The position already taken");
            } else {
                table.setRowCol(row, col);
                break;
            }
        }

    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);

    }

    private void inputContinue() {
        Scanner kb = new Scanner(System.in);
        
        while (true) {
            char con = kb.next().charAt(0);
            if (table.checkInputCon(con)) {
                System.out.println("Continue Y/N :");
            } else {
               
                break;
            }
        }
    }

    private void printContinue() {
        System.out.println("Continue Y/N :");
    }

}
