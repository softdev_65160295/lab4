/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount;
    private int row, col;
    private char con ;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {

        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row - 1;
            this.col = col - 1;
            return true;
        }

        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }

    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }

        if (checkCol()) {
            saveWin();
            return true;
        }

        if (checkX1()) {
            saveWin();
            return true;
        }

        if (checkX2()) {
            saveWin();
            return true;
        }

        return false;
    }

    private boolean checkRow() {
        return table[row][0] == table[row][1] && table[row][2] == table[row][0];
    }

    public boolean checkDraw() {
        if (turnCount == 8) {
            saveDraw();
            return true;
        }
        return false;
    }

    private void saveWin() {
        if (player1 == getCurrentPlayer()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

    private boolean checkCol() {
        return table[0][col] == table[1][col] && table[2][col] == table[0][col];
    }

    private boolean checkX1() {
        if (table[0][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        return false;
    }

    private boolean checkX2() {
        if (table[0][2] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }
        return false;
    }

    boolean checkBound(int row, int col) {
        if (row < 0 || col < 0 || row > 4 || col > 4) {
            return true;
        }
        return false;
    }

    boolean checkPosition(int row, int col) {
        if(table[row-1][col-1]!='-'){
            return true;
        }
        return false;
    }

    boolean checkContinue() {
        if (con=='Y'){
            return true;
        }
        return false;
    }

    boolean checkInputCon(char con) {
        if (con=='Y'||con=='N'){
            this.con=con;
            return false;
            
        }
        return true;
        }

    
}
